#### Description

This is a sample project to demonstrate how to build a backend API using Spring Boot.


#### Prerequisite and Installation

You have two choice for installation: either build the project and run it using Tomcat or run it using Docker.
The easieast way to accomplish the former is to install an IDE like Eclipse with Tomcat pre-installed.

* [Java 1.8+](https://www.java.com/it/download/)
* [Eclipse](https://www.eclipse.org)
* [Maven](http://maven.apache.org)
* [Tomcat](http://tomcat.apache.org)

For the latter download and install Docker and the use the following instructions (quickest and RECOMMENDED)

* [Docker](https://www.docker.com/)

#### Steps for running using Docker

* Checkout or download the project
* Use your terminal to go into the folder you downloaded the project.
* Build and start the container by running:

```
$ docker-compose up --build
```

##### Test application with command

```
$ curl localhost:8080/space
```


##### Stop Docker Container:
```
docker-compose down
```
