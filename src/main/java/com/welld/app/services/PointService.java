package com.welld.app.services;

import java.util.List;
import java.util.Set;

import com.welld.app.model.Point;

public interface PointService {

	void addPoint(Point point);
	
	Set<Point> getAllPoints();

	Set<Point> deleteSpace();

	List<Set<Point>> getAllLinesWithPoints(long n);

}
