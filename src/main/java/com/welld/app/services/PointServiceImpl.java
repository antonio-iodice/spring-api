package com.welld.app.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.welld.app.model.Line;
import com.welld.app.model.Point;

@Service
public class PointServiceImpl implements PointService {
	
	Set<Point> points = new HashSet<>();
	Set<Line> lines = new HashSet<>();
	
	@Override
	public void addPoint(Point point) {
		if (points.contains(point)) {
			return;
		}
		addToExistingLines(point);
		createNewLinesFor(point);
		points.add(point);
	}
	
	@Override
	public Set<Point> getAllPoints() {
		return points;
	}
	
	@Override
	public Set<Point> deleteSpace() {
		points.clear();
		lines.clear();
		return points;
	}
	
	@Override
	public List<Set<Point>> getAllLinesWithPoints(long n) {
		return lines.stream()
				.filter(line -> line.getPoints().size() >= n)
				.map(Line::getPoints)
				.collect(Collectors.toList());
	}
	
	private void addToExistingLines(Point newPoint) {
		for (Line line: lines) {
			if (line.contains(newPoint)) {
				line.getPoints().add(newPoint);
			}
		}
	}
	
	private void createNewLinesFor(Point newPoint) {		
		List<Line> newLines = new ArrayList<>();
		
		for (Point point: points) {
			Line newLine = Line.make(point, newPoint);
			newLine.addPoint(point);
			newLine.addPoint(newPoint);
			newLines.add(newLine);
		}
	
		lines.addAll(newLines);
	}

}
