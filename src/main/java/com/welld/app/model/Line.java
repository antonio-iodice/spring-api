package com.welld.app.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Diagonal Line => Y = m*X + b
 * Horizontal Line => Y = v
 * Vertical Line => X = v
 *
 */
public class Line {
	
	private LineType type;
	private Float m;
	private Float b;
	private Float v;
	private Set<Point> points = new HashSet<>();
	
	public static Line make(Point p0, Point p1) {
		if (p0.getX() == p1.getX()) {
			return new Line(p0.getX(), LineType.VERTICAL);
		}
		
		if (p0.getY() == p1.getY()) {
			return new Line(p1.getY(), LineType.HORIZONTAL);
		}
		
		// Slope m = (y1 - y0)/(x1 - x0)
		// y = mx + b -> b = y - mx
		float m = ((float) (p1.getY() - p0.getY())) / ( (float) (p1.getX() - p0.getX()));
		float b = p0.getY() - (m * p0.getX());
		return new Line(m, b);	
	}
	
	private Line() {}
	
	private Line(float m, float b) {
		this.m = m;
		this.b = b;
		this.type = LineType.DIAGONAL;
	}
	
	private Line(float v, LineType type) {
		this.v = v;
		this.type = type;
	}
	
	public Set<Point> getPoints() {
		return this.points;
	}
	
	public void addPoint(Point p) {
		points.add(p);
	}
	
	public boolean contains(Point p) {
		if (type == LineType.DIAGONAL) {
			return p.getY() == (this.m * p.getX()) + this.b; // y = mx + b
		}
		
		if (type == LineType.HORIZONTAL) {
			return p.getY() == this.v;
		}
		
		if (type == LineType.VERTICAL) {
			return p.getX() == this.v;
		}
		
		return false;
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((b == null) ? 0 : b.hashCode());
		result = prime * result + ((m == null) ? 0 : m.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((v == null) ? 0 : v.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Line other = (Line) obj;
		if (b == null) {
			if (other.b != null)
				return false;
		} else if (!b.equals(other.b))
			return false;
		if (m == null) {
			if (other.m != null)
				return false;
		} else if (!m.equals(other.m))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (v == null) {
			if (other.v != null)
				return false;
		} else if (!v.equals(other.v))
			return false;
		return true;
	}

	public LineType getType() {
		return type;
	}

	public Float getM() {
		return m;
	}

	public Float getB() {
		return b;
	}

	public Float getV() {
		return v;
	}
	
}
