package com.welld.app.model;

public enum LineType {
	
	DIAGONAL,
	HORIZONTAL,
	VERTICAL

}
