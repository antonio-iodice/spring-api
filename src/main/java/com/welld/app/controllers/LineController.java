package com.welld.app.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.welld.app.model.Point;
import com.welld.app.services.PointService;

@RestController
public class LineController {
	
	@Autowired
	private PointService pointService;
	
	@GetMapping(value = "/lines/{n}")
    public List<Set<Point>> allPoints(@PathVariable Long n) {
    	return pointService.getAllLinesWithPoints(n);
    }

}
