package com.welld.app.controllers;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.welld.app.model.Point;
import com.welld.app.services.PointService;

@RestController
@RequestMapping(value = "/point", produces = MediaType.APPLICATION_JSON_VALUE)
public class PointController {
	
	List<Point> points = new ArrayList<>();
	
	@Autowired
	private PointService pointService;
    
    @PostMapping
    public Set<Point> addPoint(@RequestBody Point newPoint) {
    	pointService.addPoint(newPoint);
        return pointService.getAllPoints();
    }
}
