package com.welld.app.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.welld.app.model.Point;
import com.welld.app.services.PointService;

@RestController
public class SpaceController {
	
	@Autowired
	private PointService pointService;
	
	@GetMapping(value = "/space")
    public Set<Point> getSpace() {
    	return pointService.getAllPoints();
    }
	
	@DeleteMapping(value = "/space")
	public Set<Point> deleteSpace() {
		return pointService.deleteSpace();
	}

}
