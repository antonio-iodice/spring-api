package com.welld.app.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.welld.app.Application;
import com.welld.app.model.Point;

@SpringBootTest(classes = {Application.class})
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class PointServiceTest {
	
	@Autowired
	private PointService pointService;

	@Test
	public void pointIsAddedAndRetrieved() {
		assertEquals(0, pointService.getAllPoints().size());
		Point p0 = new Point(0, 0);
		pointService.addPoint(p0);
		assertEquals(1, pointService.getAllPoints().size());
		assertTrue(pointService.getAllPoints().contains(p0));
	}
	
	@Test
	public void linesAreRetrieved() {
		Point p0 = new Point(0, 0);
		Point p1 = new Point(1, 1);
		pointService.addPoint(p0);
		pointService.addPoint(p1);
		assertEquals(1, pointService.getAllLinesWithPoints(2).size());
		assertEquals(0, pointService.getAllLinesWithPoints(3).size());
	}
	
	@Test
	public void spaceIsDeleted() {
		Point p0 = new Point(0, 0);
		Point p1 = new Point(1, 1);
		pointService.addPoint(p0);
		pointService.addPoint(p1);
		assertFalse(pointService.getAllPoints().isEmpty());
		assertFalse(pointService.getAllLinesWithPoints(2).isEmpty());
		pointService.deleteSpace();
		assertTrue(pointService.getAllPoints().isEmpty());
		assertTrue(pointService.getAllLinesWithPoints(2).isEmpty());
	}

}
