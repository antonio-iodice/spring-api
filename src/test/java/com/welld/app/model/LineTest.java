package com.welld.app.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LineTest {

	@Test
	void horizontalLineIsMade() {
		Point point0 = new Point(0, 10);
		Point point1 = new Point(1, 10);
		Line line = Line.make(point0, point1);
		assertEquals(LineType.HORIZONTAL, line.getType());
		assertEquals(10, line.getV());
		
	}
	
	@Test
	void verticalLineIsMade() {
		Point point0 = new Point(1, 5);
		Point point1 = new Point(1, 7);
		Line line = Line.make(point0, point1);
		assertEquals(LineType.VERTICAL, line.getType());
		assertEquals(1, line.getV());
	}
	
	@Test
	void diagonalLineIsMade() {
		Point point0 = new Point(1, 1);
		Point point1 = new Point(2, 2);
		Line line = Line.make(point0, point1);
		assertEquals(LineType.DIAGONAL, line.getType());
		assertEquals(1, line.getM());
		assertEquals(0, line.getB());
		
		Point point2 = new Point(0, 0);
		Point point3 = new Point(1, 2);
		Line line1 = Line.make(point2, point3);
		assertEquals(LineType.DIAGONAL, line1.getType());
		assertEquals(2, line1.getM());
		assertEquals(0, line1.getB());
	}

}
