package com.welld.app.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.welld.app.Application;
import com.welld.app.model.Point;
import com.welld.app.services.PointService;

@SpringBootTest(classes = {Application.class})
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class SpaceControllerIntegrationTest {
	
	@Autowired
    private MockMvc mvc;
 
    @Autowired
    private PointService service;

    @Test
    public void givenPoints_whenGetSpace_thenReturnPointsArray()
      throws Exception {
         
    	service.addPoint(new Point(1, 10));
        
        mvc.perform(get("/space")
          .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk())
          .andExpect(content().string(containsString("x")))
          .andExpect(jsonPath("$.*", hasSize(1)));
    }
    
    @Test
    public void givenPoints_whenDeleteSpace_thenReturnEmptyArray() throws Exception {
    	
    	service.addPoint(new Point(1, 10));
    	
    	mvc.perform(get("/space")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(1)));
        
        mvc.perform(delete("/space")
          .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk());
        
        mvc.perform(get("/space")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(0)));
    }

}
