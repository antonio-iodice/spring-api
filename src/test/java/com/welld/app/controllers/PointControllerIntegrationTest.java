package com.welld.app.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.welld.app.Application;
import com.welld.app.model.Point;

@SpringBootTest(classes = {Application.class})
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class PointControllerIntegrationTest {
	
	@Autowired
    private MockMvc mvc;
	
	@Autowired
	ObjectMapper objectMapper;

    @Test
    public void whenPostPoint_thenPointIsAdded() throws Exception {
    	
    	Point p0 = new Point(12.7f, 10.1f);
        
        mvc.perform(post("/point")
          .contentType(MediaType.APPLICATION_JSON)
          .content(objectMapper.writeValueAsString(p0)))
          .andExpect(status().isOk())
          .andExpect(content().string(containsString("x")))
          .andExpect(jsonPath("$[0].x", is(12.7)))
          .andExpect(jsonPath("$[0].y", is(10.1)));

    }

}
